Django==4.2.1
django-filter==23.2
djangorestframework==3.14.0
psycopg2==2.9.6
Pillow==9.5.0
whitenoise==6.4.0