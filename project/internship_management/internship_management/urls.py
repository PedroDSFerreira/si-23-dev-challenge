from django.contrib import admin
from django.urls import include, path
from applicants.views import StatusListView

urlpatterns = [
    path('', include('pages.urls')),
    path('api/',
        include(
            [
                path('applicants/', include('applicants.urls')),
                path('roles/', include('roles.urls')),
                path('statuses/', StatusListView.as_view(), name='statuses-list')
            ]
        )
    )
    
]
