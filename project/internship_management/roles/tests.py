from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from .models import Role


class RoleTests(TestCase):
    def setUp(self):
        self.client = APIClient()


    def tearDown(self):
        Role.objects.all().delete()

    
    def create_role(self, name):
        return Role.objects.create(name=name)


    def test_create_role(self):
        url = reverse('role-search-create')
        data = {'name': 'Developer'}
        response = self.client.post(url, data)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Assert the response data
        self.assertEqual(response.data['name'], 'Developer')


    def test_create_duplicate_role(self):
        self.create_role('Developer')

        url = reverse('role-search-create')
        data = {'name': 'Developer'}
        response = self.client.post(url, data)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_get_role(self):
        role = self.create_role('Developer')

        url = reverse('role-detail', args=[role.id])
        response = self.client.get(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(response.data['name'], role.name)


    def test_search_roles(self):
        self.create_role('Developer')
        self.create_role('Designer')
        self.create_role('Architect')

        url = reverse('role-search-create')
        response = self.client.get(url, {'search': 'Dev'})

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], 'Developer')


    def test_update_role(self):
        role = self.create_role('Developer')

        url = reverse('role-detail', args=[role.id])
        data = {'name': 'Senior Developer'}
        response = self.client.put(url, data)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(response.data['name'], 'Senior Developer')


    def test_delete_role(self):
        role = self.create_role('Developer')

        url = reverse('role-detail', args=[role.id])
        response = self.client.delete(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_search_roles_no_match(self):
        self.create_role('Developer')
        self.create_role('Designer')
        self.create_role('Architect')

        url = reverse('role-search-create')
        response = self.client.get(url, {'search': 'Engineer'})

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert no roles are returned
        self.assertEqual(len(response.data), 0)