from django.urls import path
from .views import RoleSearchCreateView, RoleDetailView


urlpatterns = [
    path('', RoleSearchCreateView.as_view(), name='role-search-create'),
    path('<int:pk>/', RoleDetailView.as_view(), name='role-detail'),
]