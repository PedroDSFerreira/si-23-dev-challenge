from rest_framework import generics, filters
from .models import Role
from .serializers import RoleSerializer

class RoleSearchCreateView(generics.ListCreateAPIView):
    """Search for roles or create a new role."""
    queryset = Role.objects.all().order_by('name')
    serializer_class = RoleSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['^name']

class RoleDetailView(generics.RetrieveUpdateDestroyAPIView):
    """Retrieve, update or delete a role."""
    queryset = Role.objects.all()
    serializer_class = RoleSerializer