from django.db.models.signals import post_migrate
from django.dispatch import receiver
from django.apps import apps
from .models import Status

@receiver(post_migrate, sender=apps.get_app_config('applicants'))
def on_post_migrate(sender, **kwargs):
    Status.objects.get_or_create(name='Approved')
    Status.objects.get_or_create(name='Rejected')
    Status.objects.get_or_create(name='Under analysis')
