from django.urls import path
from .views import ApplicantSearchCreateView, ApplicantDetailView


urlpatterns = [
    path('', ApplicantSearchCreateView.as_view(), name='applicant-search-create'),
    path('<int:pk>/', ApplicantDetailView.as_view(), name='applicant-detail'),
]