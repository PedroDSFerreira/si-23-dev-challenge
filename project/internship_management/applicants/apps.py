from django.apps import AppConfig

class ApplicantsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'applicants'

    def ready(self):
        super().ready()
        import applicants.signals  # Import the signals module
