from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from .models import Applicant, ApplicantRole, Status
from roles.models import Role



class ApplicantTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.roles = self.create_roles()
        self.statuses = Status.objects.all()


    def tearDown(self):
        ApplicantRole.objects.all().delete()
        Applicant.objects.all().delete()
        Role.objects.all().delete()


    def create_roles(self):
        roles_data = [
            {'name': 'Developer'},
            {'name': 'Tester'}
        ]
        return Role.objects.bulk_create([Role(**data) for data in roles_data])


    def test_create_applicant(self):
        url = reverse('applicant-search-create')
        data = {
            'name': 'John Doe',
            'phone_number': '123456789',
            'email': 'john@example.com',
            'roles': [
                {
                    'role': self.roles[0].name,
                    'status': self.statuses[0].name
                },
                {
                    'role': self.roles[1].name,
                    'status': self.statuses[1].name
                }
            ]
        }

        response = self.client.post(url, data, format='json')

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Assert the response data
        self.assertEqual(response.data['name'], data['name'])


    def test_get_applicant(self):
        applicant = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        ApplicantRole.objects.create(applicant=applicant, role=self.roles[0], status=self.statuses[0])
        ApplicantRole.objects.create(applicant=applicant, role=self.roles[1], status=self.statuses[1])
        url = reverse('applicant-detail', args=[applicant.id])

        response = self.client.get(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        
        # Assert the response data
        self.assertEqual(response.data['name'], applicant.name)

    def test_search_applicants(self):
        applicant_1 = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        ApplicantRole.objects.create(applicant=applicant_1, role=self.roles[0], status=self.statuses[0])
        ApplicantRole.objects.create(applicant=applicant_1, role=self.roles[1], status=self.statuses[1])
        applicant_2 = Applicant.objects.create(name='Jane Smith', phone_number='987654321', email='jane@example.com')
        ApplicantRole.objects.create(applicant=applicant_2, role=self.roles[1], status=self.statuses[1])

        url = reverse('applicant-search-create')
        data = {'search': 'John'}

        response = self.client.get(url, data)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['name'], applicant_1.name)


    def test_update_applicant(self):
        applicant = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        applicant_role = ApplicantRole.objects.create(applicant=applicant, role=self.roles[0], status=self.statuses[0])
        url = reverse('applicant-detail', args=[applicant.id])

        data = {
            'name': 'Updated Name',
            'phone_number': '987654321',
            'email': 'updated@example.com',
            'roles': [
                {
                    'id': applicant_role.id,
                    'role': self.roles[1].name,
                    'status': self.statuses[1].name
                }
            ]
        }

        response = self.client.put(url, data, format='json')

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(response.data['name'], data['name'])


    def test_delete_applicant(self):
        applicant = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        url = reverse('applicant-detail', args=[applicant.id])

        response = self.client.delete(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # Assert the response data
        self.assertEqual(Applicant.objects.count(), 0)


    def test_list_applicants(self):
        applicant_1 = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        applicant_2 = Applicant.objects.create(name='Jane Smith', phone_number='987654321', email='jane@example.com')
        url = reverse('applicant-search-create')

        response = self.client.get(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(len(response.data), 2)
        for applicant in response.data:
            self.assertIn(applicant['name'], [applicant_1.name, applicant_2.name])


    def test_get_applicant_not_found(self):
        url = reverse('applicant-detail', args=[999])  # Non-existent applicant ID

        response = self.client.get(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_search_applicants_no_results(self):
        applicant = Applicant.objects.create(name='John Doe', phone_number='123456789', email='john@example.com')
        ApplicantRole.objects.create(applicant=applicant, role=self.roles[0], status=self.statuses[0])
        ApplicantRole.objects.create(applicant=applicant, role=self.roles[1], status=self.statuses[1])

        url = reverse('applicant-search-create')
        data = {'search': 'Jane Smith'}

        response = self.client.get(url, data)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert the response data
        self.assertEqual(len(response.data), 0)


    def test_update_applicant_not_found(self):
        url = reverse('applicant-detail', args=[999])  # Non-existent applicant ID
        data = {
            'name': 'John Doe',
            'phone_number': '123456789',
            'email': 'john@example.com',
            'roles': [
                {
                    'role': self.roles[1].name,
                    'status': self.statuses[1].name
                }
            ]
        }

        response = self.client.put(url, data, format='json')

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_delete_applicant_not_found(self):
        url = reverse('applicant-detail', args=[999])

        response = self.client.delete(url)

        # Assert HTTP status code
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)