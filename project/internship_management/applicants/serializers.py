from rest_framework import serializers
from .models import Applicant, ApplicantRole, Status
from roles.models import Role
from django.db import transaction


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = '__all__'


class ApplicantRoleSerializer(serializers.ModelSerializer):
    role = serializers.SlugRelatedField(
        slug_field='name', 
        queryset=Role.objects.all())
    status = serializers.SlugRelatedField(
        slug_field='name', 
        queryset=Status.objects.all(),
        default='Under analysis')

    class Meta:
        model = ApplicantRole
        fields = ('id','role','status')
    


class ApplicantSerializer(serializers.ModelSerializer):
    roles = ApplicantRoleSerializer(many=True, read_only=True)

    class Meta:
        model = Applicant
        fields = ('id', 'name', 'phone_number', 'email', 'roles')

    @transaction.atomic
    def create(self, validated_data):
        roles_data = self.context['request'].data.get('roles', [])
        applicant = Applicant.objects.create(**validated_data)

        for role_data in roles_data:
            role = role_data.get('role')
            status = parse_status(role_data.get('status'))

            status_instance = Status.objects.get(name=status)
            role_instance = Role.objects.get(name=role)

            ApplicantRole.objects.create(applicant=applicant, role=role_instance, status=status_instance)

        return applicant
    
    def to_representation(self, instance):
        roles = ApplicantRole.objects.filter(applicant=instance)
        roles_data = []
        for role in roles:
            role_data = {
                'role': role.role,
                'status': role.status
            }
            roles_data.append(role_data)
        instance.roles = roles_data
        return super().to_representation(instance)
    
    def update(self, instance, validated_data):
        roles_data = self.context['request'].data.get('roles', [])
        instance.name = validated_data.get('name', instance.name)
        instance.phone_number = validated_data.get('phone_number', instance.phone_number)
        instance.email = validated_data.get('email', instance.email)
        instance.save()

        # Delete roles that are not in the request
        roles = ApplicantRole.objects.filter(applicant=instance)
        for role in roles:
            role_data = {
                'role': role.role.name,
                'status': role.status.name
            }
            if role_data not in roles_data:
                role.delete()

        # Update or create roles
        for role_data in roles_data:
            role = role_data.get('role')
            status = parse_status(role_data.get('status'))

            status_instance = Status.objects.get(name=status)
            role_instance = Role.objects.get(name=role)

            applicant_role = ApplicantRole.objects.filter(applicant=instance, role=role_instance).first()
            if applicant_role:
                applicant_role.status = status_instance
                applicant_role.save()
            else:
                ApplicantRole.objects.create(applicant=instance, role=role_instance, status=status_instance)

        return instance
    
def parse_status(status):
    if not status:
        return 'Under analysis'
    return status