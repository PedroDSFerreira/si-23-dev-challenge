from django.db import models
from roles.models import Role

class Status(models.Model):
    """The status of an applicant role."""
    name = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name


class Applicant(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    phone_number = models.CharField(max_length=30, blank=False, null=False, unique=True)
    email = models.EmailField(null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class ApplicantRole(models.Model):
    """The role and status an applicant is applying for."""
    applicant = models.ForeignKey(Applicant, null=False, blank=False, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, null=False, blank=False, on_delete=models.CASCADE)
    status = models.ForeignKey(Status, null=False, blank=False, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['applicant', 'role'], name='unique_applicant_role')
        ]

    def __str__(self):
        return f"{self.applicant.name} - {self.role.name} - {self.status.name}"