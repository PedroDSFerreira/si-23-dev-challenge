from rest_framework import generics, filters
from .models import Applicant, Status
from .serializers import ApplicantSerializer, StatusSerializer

class ApplicantSearchCreateView(generics.ListCreateAPIView):
    """Search for applicants or create a new applicant."""
    queryset = Applicant.objects.all().order_by('name')
    serializer_class = ApplicantSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

    def get_queryset(self):
        # Perform separate searches based on query parameters
        queryset = super().get_queryset()

        name = self.request.query_params.get('search', None)
        applicant_id = self.request.query_params.get('id', None)

        if name:
            queryset = queryset.filter(name__istartswith=name)

        if applicant_id:
            queryset = queryset.filter(id=applicant_id)

        return queryset

class ApplicantDetailView(generics.RetrieveUpdateDestroyAPIView):
    """Retrieve, update or delete an applicant."""
    queryset = Applicant.objects.all()
    serializer_class = ApplicantSerializer

class StatusListView(generics.ListAPIView):
    """List all statuses."""
    queryset = Status.objects.all()
    serializer_class = StatusSerializer