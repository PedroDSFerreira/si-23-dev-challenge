# InternConnect
## Internship Applications Management Platform

## Introduction


The aim of this platform is to build web application that helps manage different role submissions for a company.

## Technologies used

### Frontend

- HTML
- CSS (Bootstrap)
- JavaScript (jQuery)

### Backend

- Django
- PostgreSQL

## Requirements



To run the server, you need to have the following software installed:

- Docker
- Docker compose

## Getting Started



1. Clone the repository:
    
    ```bash
    git clone <repo-url>
    ```
    
2. Navigate to the project directory.
3. Build and run the Docker containers:
    
    ```bash
    docker compose up
    ```
    
    This step might be necessary to repeat, if PostgreSQL container takes too long to startup.
    
4. Applying Migrations
    
    To apply migrations to the database, follow these steps:
    
    1. Make sure your Docker containers are up and running.
    2. To attach a shell to the Django container, run:
        
        ```bash
        docker compose exec django bash
        ```
        
    3. Run the following commands to apply migrations:
        
        ```bash
        python manage.py makemigrations
        python manage.py migrate
        ```
        
        If this is the first time you are running migrations, it will create the necessary tables in the database. If you have already run migrations before, it will bring your database up-to-date with the latest changes to your models.
        
5. Once everything is up and running, you can access the web platform in your browser at **[http://localhost:8000/](http://localhost:8000/)**.

### In production

1. Before starting containers, set `DEBUG=False` in docker-compose.yml
2. After containers are up, go to Django container shell and run:

```bash
python manage.py collectstatic
```

### Run tests

Tests can be run after migration process, with:

```bash
python manage.py test
```

## API Endpoints



### Applicants

- **Endpoint:** **`/applicants/?search={applicant_name}` and `/applicants/?id={applicant_id}`**
    - **Methods:** **`GET`**, **`POST`**
    - **Description:** Allows users to retrieve a list of roles based on name or id, or create a new applicant.
- **Endpoint:** **`/applicants/{applicant_id}/`**
    - **Methods:** **`GET`**, **`PUT`**, **`PATCH`**, **`DELETE`**
    - **Description:** Allows users to get, update, or delete an applicant with the specified **`applicant_id`**.

### Roles

- **Endpoint:** **`/roles/?search={role_name}`**
    - **Methods:** **`GET`**, **`POST`**
    - **Description:** Allows users to retrieve a list of roles based on name, or create a new role.
- **Endpoint:** **`/roles/{role_id}/`**
    - **Methods:** **`GET`**, **`PUT`**, **`PATCH`**, **`DELETE`**
    - **Description:** Allows users to get, update, or delete a role with the specified **`role_id`**.

### Statuses

- **Endpoint:** **`/statuses/`**
    - **Methods:** **`GET`**
    - **Description:** Allows users to retrieve the list of all available statuses.



## Development Process



### Requisites analysis

- Endpoints required
- ER Diagram
- UI/UX

### Backend

- Create Django project and apps
- Configure Django settings
- Endpoints
    - Implement endpoints’ logic
    - Create tests
    - Test endpoints (Postman)
    - Refactor

### Frontend

- Create base html page
- Create pages
- Create forms
- Handle API requests (jQuery)
